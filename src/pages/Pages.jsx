﻿import React from 'react'
import Home from './Home'
import Header from './Header'
import Subscribe from '../components/Subscribe'

const Pages = () => {
    return (
        <div className=''>
            <Header/>
            <Home/>
            <Subscribe/>
        </div>

    )
}

export default Pages